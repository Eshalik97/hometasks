<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:h="HouseChema" xmlns:i="HouseInfo" xmlns:r="RoomsChema"
    version="1.0">
    
    <xsl:template match="/">
        <AllRooms>
            <xsl:apply-templates select=".//h:House">
                <xsl:sort select="@City" data-type="text"/>
            </xsl:apply-templates>
        </AllRooms>    
    </xsl:template>
    
    <xsl:template match="h:House">
        <xsl:apply-templates select=".//h:Blocks">
            <xsl:with-param name="hRooms" select="count(.//r:Room)"/>
            <xsl:with-param name="guest" select="sum(.//@guests)"/>
            <xsl:sort select="@number" data-type="text" />
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="h:Block">
        <xsl:param name="hRooms"/>        
        <xsl:param name="guest"/>
        <xsl:apply-templates select=".//r:Room">
            <xsl:with-param name="hRooms" select="$hRooms"/>
            <xsl:with-param name="guest" select="$guest"/>
            <xsl:with-param name="bRooms" select="count(.//r:Room)"/>
            <xsl:sort select="@nuber" data-type="number"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="r:Room">
        <xsl:param name="hRooms"/>
        <xsl:param name="bRooms"/>
        <xsl:param name="guest"/>
        <xsl:variable name="Up">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
        <xsl:variable name="Down">abcdefghijklmnopqrstuvwxyz</xsl:variable>
        <xsl:variable name="city" 
            select="translate(../../../../@City,$Down,$Up)"/>
        <xsl:variable name="addr" select="../../../../i:Address"/>
        <Room>
        <Address>
            <xsl:if test="string-length($city)">
            <xsl:value-of 
            select="concat($city,'/',$addr,'/',../../@number,'/',@nuber)"/>
            </xsl:if>
            <xsl:if test="not(string-length($city))">
                <xsl:value-of 
                    select="concat(translate(../../../@City,$Down,$Up),'/',../../../i:Address,'/',../@number,'/',@nuber)"/>
            </xsl:if>
        </Address>
            <HouseRoomsCount><xsl:value-of select="$hRooms"/></HouseRoomsCount>
            <BlockRoomsCount><xsl:value-of select="$bRooms"/></BlockRoomsCount>
            <HouseGuestsCount><xsl:value-of select="$guest"/></HouseGuestsCount>
            <GuestsPerRoomAverage><xsl:value-of select="floor($guest div $hRooms)"/></GuestsPerRoomAverage>
            <xsl:apply-templates select="@guests"/>
        </Room>
    </xsl:template>
    
    <xsl:template match="@guests">
        <xsl:choose>
            <xsl:when test=".=1">
                <Allocated Single="true" Double="false" Triple="false" Quarter="false"/>
            </xsl:when>
            <xsl:when test=".=2">
                <Allocated Single="false" Double="true" Triple="false" Quarter="false"/>
            </xsl:when>
            <xsl:when test=".=3">
                <Allocated Single="false" Double="false" Triple="true" Quarter="false"/>
            </xsl:when>
            <xsl:when test=".=4">
                <Allocated Single="false" Double="false" Triple="false" Quarter="true"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>