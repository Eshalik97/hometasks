<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
    xmlns="xml_version_1.0"
    xmlns:end="https://www.meme-arsenal.com/create/template/43024">
    
    <xsl:template match="/">
            <xsl:apply-templates select="node()"/>    
    </xsl:template>
    
    <xsl:template match="@*"> 
        <xsl:variable name="Up">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
        <xsl:variable name="Down">abcdefghijklmnopqrstuvwxyz</xsl:variable>
        <xsl:variable name="attr" select="translate(name(.),$Down,$Up)"/>
        <xsl:attribute name="{$attr}"><xsl:value-of select="."/></xsl:attribute>
    </xsl:template>
    
    <xsl:template match="Guests">
        <Peoples xmlns="xml_version_1.0"
            xmlns:end="https://www.meme-arsenal.com/create/template/43024">
            <xsl:apply-templates select="@*|node()"/>    
        </Peoples>
    </xsl:template>
    
    <xsl:template match="Guest">
        <People>
            <xsl:apply-templates select="@*|node()"/>
        </People>
    </xsl:template>

    <xsl:template match="end:Guest">
        <end:People>
            <xsl:apply-templates select="@*|node()"/>
        </end:People>
    </xsl:template>
    
    <xsl:template match="*">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="text()">
        <xsl:if test="string-length(translate(.,'‘ &#x9;&#xa;&#xd;&#x20;',''))">
        <text>
            <xsl:value-of select="."/>
        </text>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>