<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">
    
    <xsl:template match="/">
        <xsl:element name="Guests">
            <xsl:call-template name="string-separation">
                <xsl:with-param name="text"><xsl:value-of select="."/></xsl:with-param>
                <xsl:with-param name="delimiter">|</xsl:with-param>
            </xsl:call-template>
        </xsl:element>
    </xsl:template>
    
    <xsl:template name="string-separation">
        <xsl:param name="text" />
        <xsl:param name="delimiter" />
        <xsl:if test="contains($text, $delimiter)">    
            <xsl:element name="Guest">
                <xsl:call-template name="Age">
                    <xsl:with-param name="text"
                        select="substring-before($text,$delimiter)" />
                    <xsl:with-param name="delimiter" select="'/'" />
                </xsl:call-template>
            </xsl:element>
            <xsl:call-template name="string-separation">
                <xsl:with-param name="text"
                    select="substring-after($text,$delimiter)" />
                <xsl:with-param name="delimiter" select="$delimiter" />
            </xsl:call-template>	
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="Age">
        <xsl:param name="text" />
        <xsl:param name="delimiter" />
        <xsl:if test="contains($text, $delimiter)">    
            <xsl:variable name="attr" 
                select="substring-before($text,$delimiter)" />
            <xsl:attribute  name ="Age"><xsl:value-of select="$attr"/></xsl:attribute>
            <xsl:call-template name="Nationalty">
                <xsl:with-param name="text"
                    select="substring-after($text,$delimiter)" />
                <xsl:with-param name="delimiter" select="$delimiter" />
            </xsl:call-template>	
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="Nationalty">
        <xsl:param name="text" />
        <xsl:param name="delimiter" />
        <xsl:if test="contains($text, $delimiter)">    
            <xsl:variable name="attr" 
                select="substring-before($text,$delimiter)" />
            <xsl:attribute  name ="Nationalty"><xsl:value-of select="$attr"/></xsl:attribute>
            <xsl:call-template name="Gender">
                <xsl:with-param name="text"
                    select="substring-after($text,$delimiter)" />
                <xsl:with-param name="delimiter" select="$delimiter" />
            </xsl:call-template>	
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="Gender">
        <xsl:param name="text" />
        <xsl:param name="delimiter" />
        <xsl:if test="contains($text, $delimiter)">    
            <xsl:variable name="attr" 
                select="substring-before($text,$delimiter)" />
            <xsl:attribute  name ="Gender"><xsl:value-of select="$attr"/></xsl:attribute>
            <xsl:call-template name="Name">
                <xsl:with-param name="text"
                    select="substring-after($text,$delimiter)" />
                <xsl:with-param name="delimiter" select="$delimiter" />
            </xsl:call-template>	
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="Name">
        <xsl:param name="text" />
        <xsl:param name="delimiter" />
        <xsl:if test="contains($text, $delimiter)">    
            <xsl:variable name="attr" 
                select="substring-before($text,$delimiter)" />
            <xsl:attribute  name ="Name"><xsl:value-of select="$attr"/></xsl:attribute>
            <xsl:call-template name="Type">
                <xsl:with-param name="text"
                    select="substring-after($text,$delimiter)" />
                <xsl:with-param name="delimiter" select="$delimiter"/>
            </xsl:call-template>	
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="Type">
        <xsl:param name="text" />
        <xsl:param name="delimiter" />
        <xsl:if test="contains($text, $delimiter)">
            <xsl:element name="Type">
                <xsl:value-of select="substring-before($text,$delimiter)"/>
            </xsl:element>
            <xsl:call-template name="Address">
                <xsl:with-param name="text"
                    select="substring-after($text,$delimiter)" />
                <xsl:with-param name="delimiter" select="$delimiter" />
            </xsl:call-template>	
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="Address">
        <xsl:param name="text" />
        <xsl:param name="delimiter" />
        
        <xsl:element name="Profile">
            <xsl:if test="not(contains($text, $delimiter))">
                <xsl:element name="Address">
                <xsl:value-of select="$text"/>
            </xsl:element>
            </xsl:if>
            <xsl:if test="contains($text, $delimiter)">
                    <xsl:element name="Address">
                        <xsl:value-of select="substring-before($text,$delimiter)"/>
                    </xsl:element>
                    <xsl:call-template name="Email">
                <xsl:with-param name="text"
                    select="substring-after($text,$delimiter)" />
                <xsl:with-param name="delimiter" select="$delimiter" />
            </xsl:call-template>
                </xsl:if>
            </xsl:element>    
        
    </xsl:template>
    
    <xsl:template name="Email">
        <xsl:param name="text" />
        <xsl:param name="delimiter" />
        <xsl:element name="Email">
                <xsl:value-of select="$text"/>
            </xsl:element>	        
    </xsl:template>
</xsl:stylesheet>