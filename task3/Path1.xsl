<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">
    <xsl:output method="xml" omit-xml-declaration="yes" />
    <xsl:key name="NamesGroup" match="item" use="substring(@Name,1,1)"/>

    <xsl:template match="/">
        <list>
        <xsl:apply-templates 
            select="//item[generate-id(.)= generate-id(key('NamesGroup',substring(@Name,1,1)))]">
            <xsl:sort select="@Name"/>
        </xsl:apply-templates>
        </list>
    </xsl:template>
    
    <xsl:template match="item">
        <xsl:element name="{ substring(@Name,1,1)}">
            <xsl:copy-of select="key('NamesGroup',substring(@Name,1,1))"/>  
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>