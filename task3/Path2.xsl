<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">
    <xsl:key name="NodeKeys" match="node()" use="local-name(.)"/>
    <xsl:key name="AttrKeys" match="@*" use="local-name(.)"/>
    
    <xsl:template match="/">
        <text>
            <xsl:apply-templates 
                select="//node()[ local-name(.) and generate-id(.)= generate-id(key('NodeKeys',local-name(.)))]"/>
            <xsl:apply-templates 
                select="//@*[generate-id(.)= generate-id(key('AttrKeys',local-name(.)))]"/>
        </text>
    </xsl:template>
    
    <xsl:template match="node()">
        Node <xsl:value-of select="local-name(.)"/> found <xsl:value-of select="count(key('NodeKeys',local-name(.)))"/> times.
    </xsl:template>
    
    <xsl:template match="@*">
        Attribute <xsl:value-of select="local-name(.)"/> found <xsl:value-of select="count(key('AttrKeys',local-name(.)))"/> times.
    </xsl:template>
    
</xsl:stylesheet>