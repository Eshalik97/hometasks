<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:end="https://www.meme-arsenal.com/create/template/43024">
	<xsl:output method="xml"/>

	<xsl:template match="/">
		<xsl:element name="string">
			<xsl:apply-templates select="Guest"/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="Guest">
		<xsl:value-of select="@Age"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="@Nationalty"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="@Gender"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="Type"/>
		<xsl:text>/</xsl:text>
		<xsl:value-of select="Profile/Address"/>
		<xsl:if test="Profile/Email">
			<xsl:text>/</xsl:text>
			<xsl:value-of select="./*/Profile/Email"/>
		</xsl:if>
		<xsl:text>|</xsl:text>
	</xsl:template>
</xsl:stylesheet>
