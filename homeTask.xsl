<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <result>
	<xsl:apply-templates select="preceding-sibling::Guest[@Name='Jimmy']" mode="under"/>
	<xsl:text>
	--------------------------------------------
	</xsl:text>
	<xsl:apply-templates select="following-sibling::Guest[@Name='Jimmy']" mode="after"/>
	<xsl:text>
	--------------------------------------------
	</xsl:text>
    <xsl:apply-templates select="//Address"/>
  </result>
</xsl:template>
<xsl:template match="Guest" mode="under">
	<xsl:value-of select="translate(@Name,'J','j')"/>
</xsl:template>
<xsl:template match="Guest[not(@Nationalty='BY')]" mode="after">
	<xsl:value-of select="//Addres"/>
</xsl:template>

<xsl:template match="Addres[not(contains(.,'Pushkinskaya'))]">
	<xsl:element name="Addres">
		<xsl:attribute name="Nationality">
	      <xsl:value-of select="../../@Nationalty" />
		</xsl:attribute>
		<xsl:attribute name="Name">
	      <xsl:value-of select="../../@Name" />
		</xsl:attribute>
		<xsl:value-of select="."/>
	</xsl:element>
</xsl:template>
</xsl:stylesheet>